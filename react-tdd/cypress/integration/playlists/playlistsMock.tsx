export const playlistsMock = [
  { id: "123", name: "PlaylistMock 123", public: false, description: "123" },
  { id: "345", name: "PlaylistMock 345", public: false, description: "234" },
  { id: "456", name: "PlaylistMock 456", public: false, description: "345" },
];
