import { Playlist } from "../../../src/playlists/components/Playlist"


describe('PlaylistsView', () => {

  beforeEach(() => {
    cy.fixture('playlists.js').then((playlists: Playlist[]) => {

      cy.intercept('/playlists', { fixture: 'playlists.js' }).as('playlistsRequest')
      cy.intercept('/playlists/**', (req) => {
        req.reply({
          body: playlists.find(p => p.id === req.url.match('.*?/playlists/(.*)')![1])
        })
      }).as('selectedPlaylistRequest')
      cy.visit('http://localhost:3000/')
    })
  })

  it('shows list of playlists', () => {
    // title
    cy.get('.display-1')
      .should('be.visible')
      .should('have.text', 'Playlists')

    // list of playlists
    // cy.contains('PlaylistMock')
    cy.wait('@playlistsRequest')
    cy.get('.list-group-item')
      .should('have.length', 3)
      .first().should('contain.text', 'PlaylistMock 123')
  })

  it('shows selected playlist', () => {

    cy.wait('@playlistsRequest')
    cy.get('.list-group-item').should('not.have.class', 'active')
    // highlight ( .active class ) clicked
    cy.get('.list-group-item').eq(1).click()
    
    cy.wait('@selectedPlaylistRequest')
    cy.get('.list-group-item').eq(1).should('have.class', 'active')

    // selected playlist details on side 
    cy.get('[data-testid="playlist-details-component"]')
      .should('be.visible')
      .within(() => {
        cy.contains('PlaylistMock 234').should('be.visible')
        cy.findByRole('definition', { name: /Public/ }).should('be.visible')
      })
  })

  it('can edit selected playlist', () => {
    cy.get('.list-group-item').eq(1).click()
    // clicking 'Edit' show edit form
    cy.contains('Edit').click()
    cy.get('[data-testid="playlist-details-component"]')
      .should('not.exist')
    cy.get('[data-testid="playlist-form-component"]')
      .should('exist')
      .find('form').within(() => {
        // filling form validates playlist data
        cy.get('[name=name]').should('have.value', 'PlaylistMock 234')
        cy.get('[name=name]')
          .clear().type('Ala ma kota')
          .should('have.value', 'Ala ma kota')
        // clicking 'Save' updates playlist on list
        cy.contains('Save').click()
      })
    // after 'Save' form is closed
    cy.get('[data-testid="playlist-form-component"]')
      .should('not.exist')
    // after 'Save' details of saved playlist is visible
    cy.get('[data-testid="playlist-details-component"]')
      .should('exist')
  })


  it('playlist form validates invalid input', () => {
    cy.get('.list-group-item').eq(1).click()
    // clicking 'Edit' show edit form
    cy.contains('Edit').click()

    cy.get('[data-testid="playlist-form-component"]')
      .should('exist')
      .find('form').within(() => {
        // filling form validates playlist data

        cy.contains('Field is required').should('not.exist')

        cy.get('[name=name]').clear().type(' ').should('have.value', ' ')

        cy.contains('Field is required')

        // clicking 'Save' updates playlist on list
        cy.contains('Save').click()
        cy.root().closest('[data-testid="playlist-form-component"]').should('exist')

        cy.get('[name=name]')
          .clear().type('Ala ma kota')
          .should('have.value', 'Ala ma kota')
        cy.contains('Save').click()
      })
    cy.get('[data-testid="playlist-form-component"]').should('not.exist')
  })
})
