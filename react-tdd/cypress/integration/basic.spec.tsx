it('should perform basic google search', () => {
  cy.fixture('playlists').as('playlists').then(playlists => {

    cy.visit('http://localhost:3000');
    // cy.get('[name="q"]')
    //   .type('subscribe')
    //   .type('{enter}');


    cy.get('[data-testid=playlist_name]').should('not.exist')


    cy.get('.list-group-item')
      .should('have.length', playlists.length)
      .should('not.have.class', 'active')

    cy.contains('PlaylistMock 123').click().should('have.class', 'active')

    cy.get('[data-testid=playlist_name]').should('have.text', 'PlaylistMock 123')

    cy.get('button').contains('Edit').click()

    cy.get('form').within($form => {
      cy.get('[name=name]').should('have.value', playlists[0].name)
    })

    
  })
});