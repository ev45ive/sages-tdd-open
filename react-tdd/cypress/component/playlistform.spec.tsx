import * as React from 'react'
import { mount } from '@cypress/react'
import { PlaylistForm } from '../../src/playlists/components/PlaylistForm'

import 'bootstrap/dist/css/bootstrap.css'

it('Button', () => {
  const mockPlaylist = {
    description: '',
    id: '', name: '', public: false
  }
  const saveSpy = cy.spy()
  const cancelSpy = cy.spy()

  mount(<PlaylistForm playlist={mockPlaylist} onSave={saveSpy} onCancel={cancelSpy} />)

  cy.findByLabelText('Name:').clear()
  cy.findByText('Save').click().then(() => {
    expect(saveSpy).not.called
  })

  cy.findByLabelText('Name:').type('Ala ma kota')
  cy.findByText('Save').click().then(() => {
    expect(saveSpy).calledOnceWith({
      description: '',
      id: '', 
      name: 'Ala ma kota', 
      public: false
    })
  })



})