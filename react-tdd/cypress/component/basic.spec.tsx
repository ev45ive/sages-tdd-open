import * as React from 'react'
import { mount } from '@cypress/react'
import { Button } from '../../src/stories/Button'

it('Button', () => {
  const spy = cy.spy()
  mount(<Button label="Test button" onClick={spy} />)
  cy.get('button').contains('Test button').click().then(($btn)=>{
    debugger;
    expect(spy).to.have.been.called
  })

})