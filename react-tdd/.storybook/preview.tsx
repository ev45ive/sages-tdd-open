import {} from "@storybook/react";

import "bootstrap/dist/css/bootstrap.css";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

import { addDecorator } from "@storybook/react";
import { initialize , mswDecorator } from "msw-storybook-addon";

initialize();
// addDecorator(mswDecorator);

export const decorators = [
  mswDecorator
]


// export const decorators = [
//   (Story) => (
//     <div style={{ margin: "3em" }}>
//       <Story />
//     </div>
//   ),
// ];
