import { Playlist } from "../../playlists/components/Playlist";

export let PlaylistsAPIService = {

  async loadPlaylists(): Promise<Playlist[]> {
    return await (await fetch("/playlists")).json();
  },

  async loadPlaylistById(id: string): Promise<Playlist> {
    return await (await fetch("/playlists/" + id)).json();
  },
};
