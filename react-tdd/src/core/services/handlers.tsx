import { rest } from "msw";
import { playlistsMock } from "../../playlists/containers/playlistsMock";

// npx msw init public/ --save

export const handlers = [
  rest.get("/playlists", (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(playlistsMock));
  }),
  rest.get("/playlists/:id", (req, res, ctx) => {
    const id = req.params.id;
    return res(
      ctx.delay(),
      ctx.status(200),
      ctx.json(playlistsMock.find((p) => p.id === id))
    );
  }),
];
