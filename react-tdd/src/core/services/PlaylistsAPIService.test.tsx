import { playlistsMock } from "../../playlists/containers/playlistsMock";
import { PlaylistsAPIService } from "./PlaylistsAPIService";

export {};
describe("PlaylistsAPIService", () => {

  test("fetch playlists", async () => {
    const result = await PlaylistsAPIService.loadPlaylists();
    expect(result).toEqual(playlistsMock);
  });
  
  test("fetch playlist by id", async () => {
    const result = await PlaylistsAPIService.loadPlaylistById('123');
    expect(result).toEqual(playlistsMock[0]);

  });

});
