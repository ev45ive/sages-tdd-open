import React from 'react';
import { PlaylistsView } from './playlists/containers/PlaylistsView';
import 'bootstrap/dist/css/bootstrap.css'

function App() {
  return (
    <div className="App">
      <div className="container">
        <div className="row">
          <div className="col">
            <PlaylistsView />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
