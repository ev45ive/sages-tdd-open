import React from "react";
import { render, screen } from "@testing-library/react";
import { PlaylistForm } from "./PlaylistForm";
import userEvent from "@testing-library/user-event";

const setup = () => { }

test("renders PlaylistForm", async () => {
  const saveSpy = jest.fn()
  const cancelSpy = jest.fn()
  render(<PlaylistForm playlist={{
    description: '',
    id: '', name: '', public: false
  }} onSave={saveSpy} onCancel={cancelSpy} />);
  userEvent.click(await screen.findByText('save'))
  expect(saveSpy).not.toHaveBeenCalled()
  userEvent.type(await screen.findByLabelText('Name:'), 'ala ma kota')
  userEvent.click(await screen.findByText('save'))
  expect(saveSpy).toHaveBeenCalled()
});

test("renders PlaylistForm", () => {
  const saveSpy = jest.fn()
  const cancelSpy = jest.fn()
  render(<PlaylistForm playlist={{
    description: 'test',
    id: '123', name: 'test', public: false
  }} onSave={saveSpy} onCancel={cancelSpy} />);
});

test("renders PlaylistForm", () => {
  const saveSpy = jest.fn()
  const cancelSpy = jest.fn()
  render(<PlaylistForm playlist={{
    description: 'test',
    id: '123', name: 'test', public: true
  }} onSave={saveSpy} onCancel={cancelSpy} />);
});
