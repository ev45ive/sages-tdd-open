import { ComponentStory, ComponentMeta } from "@storybook/react";
import { PlaylistDetails } from "./PlaylistDetails";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Playlists/components/PlaylistDetails",
  component: PlaylistDetails,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    // backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof PlaylistDetails>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof PlaylistDetails> = (args) => (
  <PlaylistDetails {...args} />
);

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  playlist: {
    id: "123",
    name: "Playlist name",
    public: true,
    description: "playlist descr",
  },
};
