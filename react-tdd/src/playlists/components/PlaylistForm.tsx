import React, { useEffect, useState } from 'react'
import { Playlist } from './Playlist'

interface Props {
    playlist: Playlist
    onCancel(): void
    onSave(draft: Playlist): void
}

export const PlaylistForm = ({ playlist, onCancel, onSave }: Props) => {

    const [playlistName, setPlaylistName] = useState(playlist.name)
    const [errors, setErrors] = useState<any>({})

    useEffect(() => {
        const errors: any = {}

        if (playlistName.trim().length === 0) {
            errors.name = 'Field is required'
        }
        setErrors(errors)

    }, [playlistName])

    const save = () => {
        Object.values(errors).length === 0 && onSave({
            ...playlist,
            name: playlistName
        })
    }

    return (
        <div data-testid="playlist-form-component">
            <form>
                <div className="form-group mb-3">
                    <label htmlFor="playlist_name">Name:</label>
                    <input type="text" name="name" id="playlist_name" className="form-control" placeholder="Name"
                        value={playlistName}
                        onChange={e => setPlaylistName(e.target.value)} />

                    {errors.name && <p>{errors.name}</p>}
                </div>

                <div className="form-check">
                    <label className="form-check-label">
                        <input type="checkbox" className="form-check-input" name="public" id="playlist_public" />
                        Public
                    </label>
                </div>

                <button type="button" className="btn btn-danger" onClick={onCancel}>Cancel</button>
                <button type="button" className="btn btn-succeess"
                    onClick={save}>Save</button>
            </form>
        </div>
    )
}
