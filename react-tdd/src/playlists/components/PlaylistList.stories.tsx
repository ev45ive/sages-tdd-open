import { ComponentStory, ComponentMeta } from "@storybook/react";
import { PlaylistList } from "./PlaylistList";

const playlistsMock = [
  { id: "123", name: "PlaylistMock 123", public: false, description: "123" },
  { id: "345", name: "PlaylistMock 345", public: false, description: "234" },
  { id: "456", name: "PlaylistMock 456", public: false, description: "345" },
];
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Playlists/components/PlaylistList",
  component: PlaylistList,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof PlaylistList>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof PlaylistList> = (args) => (
  <PlaylistList {...args} />
);

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  playlists: playlistsMock,
};
