import React from "react";
import { Playlist } from "./Playlist";

interface Props {
  /**
   * Playlist data object
   *
   * @type {Playlist}
   * @memberof Props
   */
  playlist?: Playlist;
  /**
   * Request Edit mode
   *
   * @param {Playlist["id"]} id
   * @memberof Props
   */
  onEdit?(id: Playlist["id"]): void;
}

export const PlaylistDetails = ({
  playlist = {
    id: "",
    name: "",
    description: "",
    public: false,
  },
  onEdit,
}: Props) => {
  return (
    <div className="card" data-testid="playlist-details-component">
      <div className="card-body">
        <dl>
          <dt>Name:</dt>
          <dd data-testid="playlist_name">{playlist.name}</dd>

          <dt id="playlist_public">Public:</dt>
          <dd aria-labelledby="playlist_public">
            {playlist.public ? "Yes" : "No"}
          </dd>

          <dt id="playlist_description">Description:</dt>
          <dd aria-labelledby="playlist_description">{playlist.description}</dd>
        </dl>
      </div>
      <div className="card-footer">
        <button
          className="btn btn-info float-end"
          onClick={() => {
            onEdit?.(playlist.id);
          }}
        >
          Edit
        </button>
      </div>
    </div>
  );
};
