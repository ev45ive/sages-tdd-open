import React from "react";
import { render, screen, logRoles } from "@testing-library/react";
import { PlaylistDetails } from "./PlaylistDetails";
import renderer from "react-test-renderer";
import userEvent from "@testing-library/user-event";

describe("Playlist Details", () => {
  const setup = () => ({
    playlist: {
      id: "123",
      name: "Playlist name",
      public: true,
      description: "playlist descr",
    },
  });

  test("Edit button click requests edit mode", () => {
    const { playlist } = setup();

    const editSpy = jest.fn();
    render(<PlaylistDetails playlist={playlist} onEdit={editSpy} />);

    const btn = screen.getByRole("button", { name: "Edit" });
    // fireEvent()
    userEvent.click(btn, {});

    expect(editSpy).toHaveBeenCalledWith(playlist.id);
  });

  test("renders PlaylistDetails - labels (invariants)", () => {
    const { playlist } = setup();
    render(<PlaylistDetails playlist={playlist} />);

    screen.getByText("Name:");
    screen.getByText("Public:");
    screen.getByText("Description:");
  });

  test("renders PlaylistDetails with playlist data", () => {
    const { playlist } = setup();

    const { rerender } = render(<PlaylistDetails playlist={playlist} />);

    const playlist_name = screen.getByTestId("playlist_name");
    expect(playlist_name).toHaveTextContent(playlist.name);

    const playlist_description = screen.getByRole("definition", {
      name: "Description:",
    });
    expect(playlist_description).toHaveTextContent(playlist.description);

    playlist.name = "changed name";
    playlist.public = false;
    rerender(<PlaylistDetails playlist={playlist} />);
    expect(screen.getByTestId("playlist_name")).toHaveTextContent(
      "changed name"
    );
  });

  test("shows 'yes' or 'no' if playlist is public or not ", () => {
    const { playlist } = setup();
    const { rerender } = render(<PlaylistDetails playlist={playlist} />);

    expect(
      screen.getByRole("definition", { name: "Public:" })
    ).toHaveTextContent("Yes");

    playlist.public = false;
    rerender(<PlaylistDetails playlist={playlist} />);
    expect(
      screen.getByRole("definition", { name: "Public:" })
    ).toHaveTextContent("No");
  });

  it("renders correctly", () => {
    const { playlist } = setup();
    const tree = renderer
      .create(<PlaylistDetails playlist={playlist}></PlaylistDetails>)
      .toJSON();
    expect(tree).toMatchSnapshot();

    playlist.public = false;
    const tree2 = renderer
      .create(<PlaylistDetails playlist={playlist}></PlaylistDetails>)
      .toJSON();
    expect(tree2).toMatchSnapshot();

    /*     
    › 1 snapshot written.
    Snapshot Summary
    › 1 snapshot written from 1 test suite.

    Test Suites: 1 passed, 1 total
    Tests:       4 passed, 4 total
    Snapshots:   1 written, 1 total
    Time:        2.497 s
    ...

    Interactive Snapshot Result
 › 1 snapshot reviewed, 1 snapshot updated
    */
  });
});
