import React, { useEffect } from "react";
import { Playlist } from "./Playlist";

interface Props {
  playlists?: Playlist[];
  selectedId?: string;
  onSelect(id: Playlist["id"]): void;
}

export const PlaylistList = ({ playlists, onSelect, selectedId }: Props) => {
  useEffect(() => {});

  if (!playlists?.length) {
    return <p>No Playlists</p>;
  }

  return (
    <div>
      <div className="list-group" role="list">
        {playlists.map((playlist, index) => (
          <div
            role="listitem"
            className={
              "list-group-item" + (selectedId === playlist.id ? " active" : "")
            }
            onClick={() => onSelect(playlist.id)}
            key={playlist.id}
          >
            {index + 1}. {playlist.name}
          </div>
        ))}
      </div>
    </div>
  );
};
