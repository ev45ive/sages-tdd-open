import React from "react";
import { render, screen } from "@testing-library/react";
import { PlaylistList } from "./PlaylistList";
import { playlistsMock } from "../containers/playlistsMock";
import userEvent from "@testing-library/user-event";

const setup = ({ playlists = playlistsMock, selectedId = "" }) => {
  const selectSpy = jest.fn();
  render(
    <PlaylistList
      playlists={playlists}
      onSelect={selectSpy}
      selectedId={selectedId}
    />
  );
  return { selectSpy };
};

test("selected item is highlighted", () => {
  setup({ selectedId: playlistsMock[1].id });
  const elem = screen.getByText(new RegExp(playlistsMock[1].name));
  expect(elem).toHaveClass("active");
});

test("selecting item emits ", () => {
  const { selectSpy } = setup({});
  const elem = screen.getByText(new RegExp(playlistsMock[1].name));
  userEvent.click(elem);
  expect(selectSpy).toBeCalledWith(playlistsMock[1].id);
});

test("renders no Playlists", async () => {
  setup({ playlists: [] });
  expect(screen.getByText("No Playlists")).toBeInTheDocument();
});

test("renders list of  PlaylistList", async () => {
  setup({});
  const elems = screen.getAllByText(
    /PlaylistMock/ /*"listitem" , {
    name: /PlaylistMock/,
  } */
  );
  expect(elems).toHaveLength(playlistsMock.length);
  elems.forEach((elem, index) => {
    expect(elem).toHaveTextContent(playlistsMock[index].name);
  });

  expect(screen.queryByText("No Playlists")).not.toBeInTheDocument();
});
