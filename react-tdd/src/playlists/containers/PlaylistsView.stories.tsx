import { ComponentStory, ComponentMeta } from "@storybook/react";
import { handlers } from "../../core/services/handlers";
import { PlaylistsView } from "./PlaylistsView";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Playlists/Views/PlaylistsView",
  component: PlaylistsView,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: "color" },
  },
} as ComponentMeta<typeof PlaylistsView>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof PlaylistsView> = (args) => (
  <PlaylistsView {...args} />
);

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {};
Primary.parameters = {
  msw: handlers,
};
