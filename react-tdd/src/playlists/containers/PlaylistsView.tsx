import { loadavg } from "os";
import React, { useEffect, useState } from "react";
import { PlaylistsAPIService } from "../../core/services/PlaylistsAPIService";
import { Playlist } from "../components/Playlist";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { PlaylistList } from "../components/PlaylistList";

interface Props { }

export const PlaylistsView = (props: Props) => {
  const [playlists, setPlaylists] = useState<Playlist[]>([]);
  const [loading, setLoading] = useState(false);
  const [selected, setSelected] = useState<Playlist | undefined>();
  const [mode, setMode] = useState('details')

  const select = (id: Playlist["id"]) => {
    PlaylistsAPIService.loadPlaylistById(id).then((data) => {
      setSelected(data);
    });
  };

  const edit = () => {
    setMode('edit')
  }
  const save = () => {
    setMode('details')
  }

  const cancel = () => { setMode('details') }

  useEffect(() => {
    setLoading(true);
    PlaylistsAPIService.loadPlaylists().then((data) => {
      setPlaylists(data);
      setLoading(false);
    });
  }, []);

  return (
    <div>
      <div className="row">
        <div className="col">
          <h1 className="display-1">Playlists</h1>
        </div>
      </div>
      <div className="row">
        <div className="col">
          {loading && <p>Loading</p>}
          {!loading && (
            <>
              <PlaylistList playlists={playlists} onSelect={select} selectedId={selected?.id} />
            </>
          )}
        </div>
        <div className="col">
          {selected && mode == 'details' && (
            <>
              <PlaylistDetails playlist={selected} onEdit={edit} />
            </>
          )}   {selected && mode == 'edit' && (
            <>
              <PlaylistForm
                playlist={selected}
                onCancel={cancel}
                onSave={save} />
            </>
          )}
        </div>
      </div>
    </div>
  );
};
