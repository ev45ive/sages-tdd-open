import React from "react";
import {
  findByText,
  render,
  screen,
  waitFor,
  waitForElementToBeRemoved,
  within,
} from "@testing-library/react";
import { PlaylistsView } from "./PlaylistsView";
import { mocked } from "ts-jest/utils";
import { Playlist } from "../components/Playlist";
import userEvent from "@testing-library/user-event";
import { playlistsMock } from "./playlistsMock";
import { PlaylistsAPIService } from "../../core/services/PlaylistsAPIService";

// jest.mock("../../core/services/PlaylistsAPIService")

describe("PlaylistsView", () => {
  test("shows no playlists", async () => {
    // mocked(PlaylistsAPIService.loadPlaylists).mockResolvedValue([]);
    render(<PlaylistsView />);

    await waitForElementToBeRemoved(() => screen.getByText("Loading"));
    // await screen.findByText("Loading")
    // await waitFor(() => screen.getByText("Loading"));
  });

  test("loads and shows list of playlists", async () => {
    // mocked(PlaylistsAPIService.loadPlaylists).mockResolvedValue(playlistsMock);

    render(<PlaylistsView />);

    await screen.findByText("Loading");
    // expect(PlaylistsAPIService.loadPlaylists).toBeCalled();

    await screen.findAllByText(/PlaylistMock/);

    expect(screen.queryByText("No Playlists")).not.toBeInTheDocument();
  });

  test("loads selected playlist details", async () => {
    // mocked(PlaylistsAPIService.loadPlaylists).mockResolvedValue(playlistsMock);

    render(<PlaylistsView />);
    const items = await screen.findAllByText(/PlaylistMock/);

    expect(
      screen.queryByTestId("playlist-details-component")
    ).not.toBeInTheDocument();

    const index = 1;
    userEvent.click(items[index]);

    const details = await screen.findByTestId("playlist-details-component");
    // within(details, { findByText }).
    const detials_name = await findByText(
      details,
      RegExp(playlistsMock[index].name)
    );
    expect(detials_name).toBeInTheDocument();
  });
});
