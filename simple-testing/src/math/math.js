// @ts-check

/**
 * Add 2 numbers
 * @export
 * @param {number} a
 * @param {number} b
 * @return {number} 
 */
export function add(a, b) {
    return a + b;
}

/** @type string | number */
let x = '123'
x = 123

// Parameter 'a' implicitly has an 'any' type
export function substract(a, b) {
    return a - b;
}
substract('a', 'b').toFixed() // OK

// / @ts-expect-error
// add(1, '2').toFixed()

// @ts-ignore
// add(1, '2').toFixed()

// x = '-2'
// add(1,x).toFixed()


// function add(a, b) {
//     if(typeof x !== 'number') throw 'Invalid input'
//     return a + b;
// }

// x = '-2';
// add(1,x).toFixed()