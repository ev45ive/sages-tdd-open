import { add } from "./math"

describe('Module Math', () => {
    describe('"add" operation', () => {

        // for (var x = 0; x < 10; x++) {
        //     it(`should add numbers ${x}`, () => {
        //         expect(add(1, x)).toBe(x +1);
        //     })
        // }

        it('should substract numbers', () => {
            expect(add(1, -3)).toBe(-2)
        })

        // it('shouldnt work on strings', () => {
        //     expect(() => add(1, '-3')).toThrow()
        // })
    })
})


// add(1,'2')
// add(1,'-2')
// add(1,'placki')