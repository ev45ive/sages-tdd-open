export type Control = {
    value: string;
};

export type ValidatorError = {
    required?: boolean;
    minlength?: number;
};

/**
 * Validate form control is not empty
 *
 * @param {{value:string}} control
 * @return {ValidatorError | null}
 */
export declare function validate(control: {
    value: string;
}): ValidatorError | null;


/**
 * checks min length
 *
 * @param {Control} control
 * @param {number} minLength
 * @return {ValidatorError | null}
 */
export declare function minLength(control: Control, minLength: number): ValidatorError | null;

