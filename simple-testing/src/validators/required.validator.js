// @ts-check

/** @type {import('./required.validator').validate} */
export function validate(control) {
    return control.value !== '' ? { required: true } : null
}

// validate({ value: '123' })?.required


/** @type {import('./required.validator').minLength} */
export function minLength(control, minLength) {
    return control.value.length < minLength ? { minlength: minLength } : null
}

// minLength({ value: '213' }, 2)?.minlength