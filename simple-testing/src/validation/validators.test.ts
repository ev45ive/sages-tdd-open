import { ValidationError } from "../../node_modules/webpack/types";
import { AsyncValidator, runAsyncValidators } from "./AsyncValidator"
import { debounce } from "./debounce";
import { minLength, required, runValidators, Validator, ValidatorError } from "./validators"
describe('Validators', () => {

    describe('Required', () => {
        it('validate empty values ', () => {
            expect(required({ value: '' })).toEqual({ required: true })
            expect(required({ value: '1' })).toBe(null)
        })
    })

    describe('Minlength', () => {
        it('validate minimum length ', () => {
            const validator = minLength(3)

            // expect(validator({ value: '' })).toEqual({ minlength: 3 })
            expect(validator({ value: '' })).toEqual(null)
            expect(validator({ value: '12' })).toEqual({ minlength: 3 })
            expect(validator({ value: '123' })).toBe(null)
        })

        it('validate minimum length and shows required length on error ', () => {
            const validator = minLength(3)
            expect(validator({ value: '2' })).toEqual({ minlength: 3 })
            const validator2 = minLength(5)
            expect(validator2({ value: '123' })).toEqual({ minlength: 5 })
        })
    })


    describe('Validators group', () => {

        const setup = ({ value = 'example' }) => {
            return {
                EmptyControl: { value: '' },
                ExampleValueControl: { value: value },
                NoErrorMock: jasmine.createSpy<Validator>('NoError').and.returnValue(null),
                RequiredMock: jasmine.createSpy<Validator>('Required').and.returnValue({ required: true }),
                FakeErrorMock: jasmine.createSpy<Validator>('Fake').and.returnValue({ Fake: true }),
            }
        }

        it('shows all failing validation error codes', () => {
            const {
                NoErrorMock, RequiredMock, FakeErrorMock, ExampleValueControl
            } = setup({ value: 'someval' })

            const validators = [
                NoErrorMock, RequiredMock, FakeErrorMock
            ]
            const result = runValidators(validators, ExampleValueControl)

            expect(NoErrorMock).toHaveBeenCalledOnceWith(ExampleValueControl)
            expect(result).toEqual({ required: true, Fake: true })
        })
        it('null result when no validators errors', () => {
            const { NoErrorMock } = setup({})

            const validators = [NoErrorMock, NoErrorMock]
            const result = runValidators(validators, { value: '' })
            expect(result).toEqual(null)
        })

    })


    describe('Async validation', () => {
        const setup = ({ value = '' }) => {
            return {
                control: { value }
            }
        }

        it('testing with callbacks', (done) => {
            setTimeout(() => {
                expect(true).toBe(true)
                done()
            }, 100)
        })

        it('report success from async sources (ie. server)', () => {
            const { control } = setup({})
            const mockSuccessValidator = jasmine.createSpy<AsyncValidator>('AsyncValidator').and.resolveTo(null)
            const resultOK = runAsyncValidators([mockSuccessValidator], control)
            return expectAsync(resultOK).toBeResolvedTo(null)
        })

        it('report errors from async sources (ie. server)', async () => {
            // expect(mockSuccessValidator).toHaveBeenCalledWith(control)
            const { control } = setup({})

            const fakeError = { mockValidator: true }
            const mockErrorValidator = jasmine.createSpy<AsyncValidator>('AsyncValidator').and.resolveTo(fakeError)
            const mockSuccessValidator = jasmine.createSpy<AsyncValidator>('AsyncValidator').and.resolveTo(null)

            const resultErr = runAsyncValidators([mockSuccessValidator, mockErrorValidator], control)
            await expectAsync(resultErr).toBeResolvedTo(fakeError)

            const resultErr2 = runAsyncValidators([mockErrorValidator, mockSuccessValidator], control)
            await expectAsync(resultErr2).toBeResolvedTo(fakeError)

            const resultSuccess = runAsyncValidators([mockSuccessValidator, mockSuccessValidator], control)
            await expectAsync(resultSuccess).toBeResolvedTo(null)
        })
    })

    describe('Rate limiting', () => {

        // beforeEach(function () {
        //     jasmine.clock().install();
        // });

        // afterEach(function () {
        //     jasmine.clock().uninstall();
        // });

        it('debounce callback for 300ms', () => {
            jasmine.clock().install();
            const spy = jasmine.createSpy('Validation').and.returnValue('errors')

            const debounced = debounce(300)

            debounced('Ca', spy)
            jasmine.clock().tick(100);
            expect(spy).not.toHaveBeenCalled()
            debounced('Cat', spy)
            jasmine.clock().tick(310);
            expect(spy).toHaveBeenCalledOnceWith('Cat')

            jasmine.clock().tick(100);
            debounced('Cats', spy)
            expect(spy).toHaveBeenCalledTimes(1)
            jasmine.clock().tick(300);

            expect(spy).toHaveBeenCalledTimes(2)
            expect(spy).toHaveBeenCalledWith('Cats')
            jasmine.clock().uninstall();
        })

        xit('delays validation until user stops typing for 400ms', async () => {
            const control = { value: '' }
            const tick = (ms: number) => new Promise(resolve => setTimeout(resolve, ms / 100))
            const spy = jasmine.createSpy<(result: ValidatorError | null) => any>('result')
            let validation = new Validation()

            // 1. Arrange
            const mockSuccessValidator = jasmine.createSpy<AsyncValidator>('AsyncValidator').and.resolveTo(null)
            validation.setValidators([mockSuccessValidator])

            // 2. Act
            validation.validate('C').then(spy)
            await tick(100)

            expect(spy).not.toHaveBeenCalled()
            validation.validate('Ca').then(spy)
            await tick(100)

            expect(spy).not.toHaveBeenCalled()
            validation.validate('Cat').then(spy)
            await tick(400)

            // 3. Assert
            expect(spy).toHaveBeenCalledTimes(1)

            // 2. Act
            await tick(400)
            expect(spy).toHaveBeenCalledTimes(1)

            validation.validate('Cats').then(spy)
            expect(spy).toHaveBeenCalledTimes(1)

            // 3. Assert
            await tick(400)
            expect(spy).toHaveBeenCalledTimes(2)


        })

    })
})


export class Validation {
    private _handler;
    private _valdators: jasmine.Spy<AsyncValidator>[];
    private _promise;

    validate(value: string): Promise<ValidatorError | null> {
        clearTimeout(this._handler)
        return new Promise(resolve => {
            this._handler = setTimeout(() => {
                // this._valdators
                resolve(null)
            }, 300)
        })
    }
    setValidators(validators: jasmine.Spy<AsyncValidator>[]) {
        this._valdators = validators
    }

}