export const debounce = (ms: number) => {
    let handle;
    return (value, cb: Function) => {
        clearTimeout(handle);
        handle = setTimeout(() => {
            cb(value);
        }, ms);
    };
};
