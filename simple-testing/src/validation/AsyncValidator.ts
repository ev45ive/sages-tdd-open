import { Control, ValidatorError } from "./validators";

export interface AsyncValidator {
    (control: Control): Promise<ValidatorError | null>;
}

export function runAsyncValidators(validators: any[], control: Control): Promise<ValidatorError | null> {
    return Promise.all(validators.map(validator => validator(control))).then(results => {
        let errors: ValidatorError | null = null
        // for (let error of results) {
        //     if (error) {
        //         errors = { ...(errors || {}), ...error }
        //     }
        // }
        errors = results.reduce((errors, error) => {
            return error ? { ...errors, ...error } : errors
        }, null)
        return errors
    })

    // return validators[0](control)
    // return Promise.resolve(null)
    // return Promise.resolve({ mockValidator: true })
}
