import { ValidationError } from "../../node_modules/webpack/types";

export type Control = {
    value: string;
};

export interface ValidatorError {
    [key: string]: any;
};

export type Validator = (c: Control) => ValidatorError | null

export function runValidators(validators: Validator[], control: Control) {
    let errors: ValidatorError | null = null
    for (let validator of validators) {
        const error = validator(control)
        if (error) {
            errors = { ...(errors || {}), ...error }
        }
    }
    return errors
}


export interface ValidatorError {
    required?: boolean;
};

export function required(control: Control): ValidatorError | null {
    return control.value === '' ? { required: true } : null
}
export interface ValidatorError {
    minlength?: number;
};
export function minLength(minLength: number) {
    return (control: Control): ValidatorError | null => {
        // return control.value.length < minLength ? { minlength: minLength } : null
        return control.value.length !== 0 && control.value.length < minLength ? { minlength: minLength } : null
    }
}
