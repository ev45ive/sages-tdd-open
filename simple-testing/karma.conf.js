// Karma configuration
// Generated on Thu Oct 28 2021 11:00:28 GMT+0200 (czas środkowoeuropejski letni)
const path = require('path')

module.exports = function (config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://www.npmjs.com/search?q=keywords:karma-adapter
    frameworks: ['jasmine', 'webpack'],


    // list of files / patterns to load in the browser
    files: [
      'src/**/*.test.js',
      'src/**/*.test.ts',
      // { pattern: 'src/**/*.test.js', type:'module', included: false },
    ],


    // list of files / patterns to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://www.npmjs.com/search?q=keywords:karma-preprocessor
    preprocessors: {
      // "src/**/*.js": ["karma-typescript", 'coverage'],
      // "src/**/*.test.js": ["karma-typescript"],
      'src/**/*.test.js': ['webpack', 'sourcemap'],
      'src/**/*.test.ts': ['webpack', 'sourcemap'],
    },

    webpack: {
      resolve: {
        extensions: ['', '.js', '.ts', '.tsx']
      },
      module: {
        rules: [
          { test: /\.tsx?$/, loader: 'ts-loader' },
          // {
          //   test: /\.tsx?$/,
          //   // exclude: [path.resolve(__dirname, "src/**/*.test.js")],
          //   enforce: 'post',
          //   use: {
          //     loader: 'istanbul-instrumenter-loader',
          //     options: { esModules: true }
          //   }
          // }
        ]
      },
      stats: {
        colors: true,
        modules: true,
        reasons: true,
        errorDetails: true
      },
      devtool: 'inline-source-map',
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://www.npmjs.com/search?q=keywords:karma-reporter
    reporters: ['progress', 'kjhtml'/* , 'coverage-istanbul' */],


    coverageIstanbulReporter: {
      reports: ['html', 'text-summary', 'lcovonly'],
      dir: path.join(__dirname, 'coverage'),
      fixWebpackSourcePaths: true,
      'report-config': {
        html: { outdir: 'html' }
      }
    },

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://www.npmjs.com/search?q=keywords:karma-launcher
    browsers: [
      'Chrome',
      //  'ChromeHeadless'
    ],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser instances should be started simultaneously
    concurrency: Infinity
  })
}
