# GIT

cd ..
git clone https://bitbucket.org/ev45ive/sages-tdd-open.git sages-tdd-open
cd sages-tdd-open
cd simple-testing
npm i
npm test

<!-- Update -->

git stash -u
git pull -f

<!-- Jak blad -->

git pull -u origin master

## Instalacje

node -v
v14.17.0

npm -v
6.14.6

git --version
git version 2.31.1.windows.1

code --version
1.61.2

Google Chrome
95.0.4638.54

## narzedzia

- js / angular / react / vue
- jasmine - jest

- selenium / codecept.io
- cypress

## Eslint

https://eslint.org/docs/user-guide/getting-started

npm install eslint --save-dev
npx eslint --init
√ How would you like to use ESLint? · problems  
√ What type of modules does your project use? · none
√ Which framework does your project use? · none
√ Does your project use TypeScript? · No / Yes
√ Where does your code run? · browser
√ What format do you want your config file to be in? · JSON
The config that you've selected requires the following dependencies:

@typescript-eslint/eslint-plugin@latest @typescript-eslint/parser@latest
√ Would you like to install them now with npm? · No / Yes
Installing @typescript-eslint/eslint-plugin@latest, @typescript-eslint/parser@latest

# Typescript

Typescript dla istniejacych projektow:
tsc --init --strict false --module esnext --allowJs true

Generowanie deklaracji dla istniejacego js
tsc -d --allowJS src/validators/required.validator.js

## Karma + Jasmine

echo 'node_modules' > .gitignore

npm install karma-cli
npm install karma karma-jasmine karma-chrome-launcher jasmine-core --save-dev

## Karma CLI init

karma init

<!-- lub awaryjnie -->

npx karma-cli init

Which testing framework do you want to use ?
Press tab to list possible options. Enter to move to the next question.

> jasmine

Do you want to use Require.js ?
This will add Require.js plugin.
Press tab to list possible options. Enter to move to the next question.

> no

Do you want to capture any browsers automatically ?
Press tab to list possible options. Enter empty string to move to the next question.

> Chrome
> ChromeHeadless

What is the location of your source and test files ?
You can use glob patterns, eg. "js/*.js" or "test/\*\*/*Spec.js".
Enter empty string to move to the next question.

> \*_/_.test.js
> 28 10 2021 11:00:11.083:WARN [init]: There is no file matching this pattern.

>

Should any of the files included by the previous patterns be excluded ?
You can use glob patterns, eg. "\*_/_.swp".
Enter empty string to move to the next question.

>

Do you want Karma to watch all the files and run the tests on change ?
Press tab to list possible options.

> yes

Config file generated at "C:\Projects\szkolenia\sages-tdd-js-open\simple-testing\karma.conf.js".

karma start

<!-- lub awaryjnie -->

npx karma-cli start

## raportowanie

npm install karma-jasmine-html-reporter --save-dev

https://www.npmjs.com/package/karma-typescript
https://www.npmjs.com/package/karma-jasmine-diff-reporter
https://www.npmjs.com/package/jasmine-snapshot
https://www.npmjs.com/package/karma-image-snapshot

https://www.npmjs.com/package/karma-coverage
https://www.npmjs.com/package/karma-coverage-istanbul-reporter
https://www.npmjs.com/package/karma-story-reporter
https://www.npmjs.com/package/karma-junit-reporter

## Parametrized test

https://www.npmjs.com/package/jasmine-parameterized

## React TDD

npx create-react-app react-tdd --template=typescript
npx sb init
npm i bootstrap react-router-dom @types/react-router-dom

## Playlists View

mkdir -p src/core/components
mkdir -p src/playlists/containers
mkdir -p src/playlists/components

code src/playlists/containers/PlaylistsView.tsx
code src/playlists/containers/PlaylistsView.stories.tsx
code src/playlists/containers/PlaylistsView.test.tsx

code src/playlists/components/PlaylistList.tsx
code src/playlists/components/PlaylistList.stories.tsx
code src/playlists/components/PlaylistList.test.tsx

code src/playlists/components/PlaylistDetails.tsx
code src/playlists/components/PlaylistDetails.stories.tsx
code src/playlists/components/PlaylistDetails.test.tsx

code src/playlists/components/PlaylistForm.tsx
code src/playlists/components/PlaylistForm.stories.tsx
code src/playlists/components/PlaylistForm.test.tsx

## MSW

https://mswjs.io/docs/getting-started

## Cypress
cd react-tdd 
<!-- package.json -> scripts... -->
npm start 
npx cypress open
npm run cy 


# Mock data 
https://github.com/marak/Faker.js/
https://github.com/boo1ean/casual

casual.seed(123);

## Cypress component testing vs cypress + storybook

https://github.com/NicholasBoll/cypress-storybook

